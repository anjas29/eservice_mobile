package me.projects.eservice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.projects.eservice.R;

/**
 * Created by me on 10/11/16.
 */

public class ListAlatAdapter extends RecyclerView.Adapter<ListAlatViewHolder>{
    private Context context;
    public static ArrayList<Alat> itemList;

    public ListAlatAdapter(Context context, ArrayList<Alat> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListAlatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_news, null);
        ListAlatViewHolder view = new ListAlatViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListAlatViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getNama());
        holder.shorTitle.setText(itemList.get(position).getTanggal());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
