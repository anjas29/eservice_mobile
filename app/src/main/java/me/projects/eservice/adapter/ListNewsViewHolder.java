package me.projects.eservice.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.projects.eservice.R;

public class ListNewsViewHolder extends RecyclerView.ViewHolder{
    public TextView title, shorTitle;
    public ImageView image;

    public CardView cardView;

    public ListNewsViewHolder(final View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.title);
        shorTitle= (TextView) v.findViewById(R.id.shortTitle);
    }
}
