package me.projects.eservice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Random;

import me.projects.eservice.R;

/**
 * Created by me on 10/11/16.
 */

public class ListNewsAdapter  extends RecyclerView.Adapter<ListNewsViewHolder>{
    private Context context;
    private ArrayList<Peminjaman> itemList;

    public ListNewsAdapter(Context context, ArrayList<Peminjaman> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListNewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_news, null);
        ListNewsViewHolder view = new ListNewsViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListNewsViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getNama());
        holder.shorTitle.setText(itemList.get(position).getTanggal());

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
