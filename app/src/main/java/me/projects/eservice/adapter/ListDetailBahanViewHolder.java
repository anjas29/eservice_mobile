package me.projects.eservice.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.projects.eservice.R;

public class ListDetailBahanViewHolder extends RecyclerView.ViewHolder{
    public TextView title, shortTitle, shortTitle2;
    public ImageView image;

    public CardView cardView;

    public ListDetailBahanViewHolder(final View v) {
        super(v);
        title = v.findViewById(R.id.title);
        shortTitle = v.findViewById(R.id.shortTitle);
        shortTitle2 = v.findViewById(R.id.shortTitle2);
    }
}
