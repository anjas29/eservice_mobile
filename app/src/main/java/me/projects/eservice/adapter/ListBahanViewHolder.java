package me.projects.eservice.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import me.projects.eservice.DetailBahanActivity;
import me.projects.eservice.DetailBarang;
import me.projects.eservice.R;

public class ListBahanViewHolder extends RecyclerView.ViewHolder{
    public TextView title, shorTitle;
    public ImageView image;

    public ListBahanViewHolder(final View v) {
        super(v);
        title = (TextView) v.findViewById(R.id.title);
        shorTitle= (TextView) v.findViewById(R.id.shortTitle);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(v.getContext(), DetailBahanActivity.class);
                intent.putExtra("id", ListBahanAdapter.itemList.get(getAdapterPosition()).getId());
                intent.putExtra("nama", ListBahanAdapter.itemList.get(getAdapterPosition()).getNama());
                v.getContext().startActivity(intent);
            }
        });
    }
}
