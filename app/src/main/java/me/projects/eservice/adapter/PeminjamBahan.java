package me.projects.eservice.adapter;

public class PeminjamBahan {
    String id;
    String nama;
    String tanggal;
    String jumlah;

    public PeminjamBahan(String id, String nama, String tanggal, String jumlah) {
        this.id = id;
        this.nama = nama;
        this.tanggal = tanggal;
        this.jumlah = jumlah;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}
