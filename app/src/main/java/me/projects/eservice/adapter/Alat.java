package me.projects.eservice.adapter;

public class Alat {
    String id;
    String nama;
    String tanggal;

    public Alat(String id, String nama, String tanggal) {
        this.id = id;
        this.nama = nama;
        this.tanggal = tanggal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
