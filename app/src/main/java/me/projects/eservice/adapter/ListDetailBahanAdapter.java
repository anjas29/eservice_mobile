package me.projects.eservice.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.projects.eservice.R;

/**
 * Created by me on 10/11/16.
 */

public class ListDetailBahanAdapter extends RecyclerView.Adapter<ListDetailBahanViewHolder>{
    private Context context;
    public static ArrayList<PeminjamBahan> itemList;

    public ListDetailBahanAdapter(Context context, ArrayList<PeminjamBahan> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public ListDetailBahanViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_detail_bahan, parent, false);
        ListDetailBahanViewHolder view = new ListDetailBahanViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(ListDetailBahanViewHolder holder, int position) {
        holder.title.setText(itemList.get(position).getNama());
        holder.shortTitle.setText(itemList.get(position).getTanggal());
        holder.shortTitle2.setText(itemList.get(position).getJumlah());
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
