package me.projects.eservice;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DetailPeminjamanActivity extends AppCompatActivity {
    String code, nis, nama, satuan, jumlah, tipe;
    Button submitButton;
    TextView namaView, alatView, tanggalView, jenisView, captionTanggalView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_peminjaman);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        code = getIntent().getStringExtra("code");
        nis = getIntent().getStringExtra("nis");
        nama = getIntent().getStringExtra("nama");

        submitButton = findViewById(R.id.submitButton);
        jenisView = findViewById(R.id.jenisView);

        jumlah = "0";
        postCheckBarang();
    }

    public void postPeminjaman(){
        //Perhatikan Method.POST dan alamat webservice
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://eservice.gesaang.com/api/peminjaman",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("DEBUGS", response);
                        try{
                            JSONObject object = new JSONObject(response);
                            boolean status = object.getBoolean("success");

                            if(status){
                                Toast.makeText(DetailPeminjamanActivity.this,"Berhasil, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                                finish();
                            }else{
                                Toast.makeText(DetailPeminjamanActivity.this,"Gagal, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                            }

                        }catch (Exception e){
                            Log.d("DEBUGS", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("DEBUGS", error.toString());

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //menambahkan paramter email dan password
                params.put("qrcode", code);
                params.put("nis", nis);
                params.put("jumlah", jumlah);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void postCheckBarang(){
        //Perhatikan Method.POST dan alamat webservice
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://eservice.gesaang.com/api/alat",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("DEBUGS", response);
                        try{
                            JSONObject object = new JSONObject(response);
                            boolean status = object.getBoolean("success");

                            if(status){
                                JSONObject data = object.getJSONObject("data");

                                namaView = findViewById(R.id.namaView);
                                alatView = findViewById(R.id.alatView);

                                tipe = object.getString("tipe");

                                if(!tipe.equals("Bahan")){
                                    tanggalView = findViewById(R.id.tanggalView);
                                    captionTanggalView = findViewById(R.id.textView7);
                                    Calendar cal = Calendar.getInstance();
                                    cal.add(Calendar.DAY_OF_MONTH, 1);

                                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                                    String formatted = format.format(cal.getTime());
                                    tanggalView.setText(formatted);

                                    tanggalView.setVisibility(View.VISIBLE);
                                    captionTanggalView.setVisibility(View.VISIBLE);
                                    if(tipe.equals("Media")){
                                        jenisView.setText("Peminjaman Media");
                                    }else {
                                        jenisView.setText("Peminjaman Alat");
                                    }
                                }else{
                                    satuan = data.getString("satuan");
                                    jenisView.setText("Pengeluaran Bahan");
                                }

                                namaView.setText(nama);
                                alatView.setText(data.getString("nama"));

                                submitButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if(tipe.equals("Bahan")){
                                            bahanDialog();
                                        }else{
                                            nonBahanDialog();
                                        }

                                    }
                                });
                            }else{
                                Toast.makeText(DetailPeminjamanActivity.this,"Gagal, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                                setContentView(R.layout.alat_notfound);
                                TextView description = findViewById(R.id.description);
                                description.setText(object.getString("description"));
                            }

                        }catch (Exception e){
                            Log.d("DEBUGS", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("DEBUGS", error.toString());

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("qrcode", code);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void nonBahanDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailPeminjamanActivity.this);

        builder.setTitle("Konfirmasi");
        builder.setMessage("Ijinkan peminjaman barang ini?");

        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                postPeminjaman();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void bahanDialog(){
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.jumlah_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(DetailPeminjamanActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputNote = view.findViewById(R.id.note);
        TextView satuanView = view.findViewById(R.id.satuan);
        satuanView.setText("*dalam "+satuan);

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show toast message when no text is entered
                if (TextUtils.isEmpty(inputNote.getText().toString())) {
                    Toast.makeText(DetailPeminjamanActivity.this, "Masukan Jumlah!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    alertDialog.dismiss();
                }
                jumlah = inputNote.getText().toString();
                postPeminjaman();
            }
        });
    }
}
