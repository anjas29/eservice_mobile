package me.projects.eservice;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailPengembalianActivity extends AppCompatActivity {
    String code;
    ProgressBar progressBar;
    ImageView imageView;
    TextView textDenda, textPengembalian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pengembalian);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        code = getIntent().getStringExtra("code");
        progressBar = findViewById(R.id.progressBar);
        textDenda = findViewById(R.id.dendaView);
        textPengembalian = findViewById(R.id.textView3);
        imageView = findViewById(R.id.camera);

        postPengembalian();
    }

    public void postPengembalian(){
        //Perhatikan Method.POST dan alamat webservice
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://eservice.gesaang.com/api/pengembalian",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("DEBUGS", response);
                        progressBar.setVisibility(View.GONE);
                        textDenda.setVisibility(View.VISIBLE);
                        textPengembalian.setVisibility(View.VISIBLE);
                        imageView.setVisibility(View.VISIBLE);

                        try{
                            JSONObject object = new JSONObject(response);
                            boolean status = object.getBoolean("success");
                            if(status){
                                TextView denda = findViewById(R.id.dendaView);
                                denda.setText("Denda : Rp. "+object.getString("denda")+",-");
                                Toast.makeText(DetailPengembalianActivity.this,"Berhasil, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                            }else{
                                setContentView(R.layout.alat_notfound);
                                TextView descriptionView = findViewById(R.id.description);
                                descriptionView.setText(object.getString("description"));
                                Toast.makeText(DetailPengembalianActivity.this,"Gagal, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                            }
                        }catch (Exception e){
                            Log.d("DEBUGS", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("DEBUGS", error.toString());

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                //menambahkan paramter email dan password
                params.put("qrcode", code);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
