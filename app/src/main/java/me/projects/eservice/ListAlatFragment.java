package me.projects.eservice;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.projects.eservice.adapter.Alat;
import me.projects.eservice.adapter.ListAlatAdapter;
import me.projects.eservice.adapter.ListNewsAdapter;
import me.projects.eservice.adapter.Peminjaman;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListAlatFragment extends Fragment {
    private RecyclerView recyclerView;
    public static ArrayList<Alat> itemList;
    private LinearLayoutManager layoutManager;
    Context context;

    public ListAlatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_alat, container, false);
        context = view.getContext();
        recyclerView = (RecyclerView)view.findViewById(R.id.list_view);
        layoutManager= new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        postCheckAlat();

        return view;
    }

    public void postCheckAlat(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://eservice.gesaang.com/api/list-alat", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("DEBUGS", response.toString());
                try {
                    JSONArray data = response.getJSONArray("data");
                    itemList = new ArrayList<>();

                    for(int i=0;i<data.length();i++){
                        String id = data.getJSONObject(i).getString("id");
                        String nama = data.getJSONObject(i).getString("nama");
                        String stock = data.getJSONObject(i).getString("stock");
                        String total = data.getJSONObject(i).getJSONArray("buku").length()+"";
                        String jumlah = "Jumlah tersedia "+stock+" dari "+ total;
                        itemList.add(new Alat(id,nama,jumlah));
                    }

                    ListAlatAdapter adapter = new ListAlatAdapter(context, itemList);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setHasFixedSize(true);

                    recyclerView.setLayoutManager(layoutManager);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("DEBUGS", "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

}
