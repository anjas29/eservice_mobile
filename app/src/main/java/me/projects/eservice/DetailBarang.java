package me.projects.eservice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import me.projects.eservice.adapter.Alat;
import me.projects.eservice.adapter.ListAlatAdapter;
import me.projects.eservice.adapter.ListDetailAlatAdapter;

public class DetailBarang extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static ArrayList<Alat> itemList;
    private LinearLayoutManager layoutManager;
    String id, nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);
        recyclerView = (RecyclerView)findViewById(R.id.list_view);
        layoutManager= new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        id = getIntent().getExtras().getString("id");
        nama = getIntent().getExtras().getString("nama");

        getSupportActionBar().setTitle(nama);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        postCheckDetailBarang();
    }

    public void postCheckDetailBarang(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://eservice.gesaang.com/api/list-alat/"+id, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("DEBUGS", response.toString());
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONArray buku = data.getJSONArray("buku");

                    itemList = new ArrayList<>();
                    if(buku.length()>0){
                        String id = data.getString("id");
                        for(int i=0;i<buku.length();i++){
                            JSONArray peminjaman = buku.getJSONObject(i).getJSONArray("peminjaman");
                            if(peminjaman.length()>0){
                                String nama = peminjaman.getJSONObject(0).getJSONObject("siswa").getString("nama");
                                String kelas = peminjaman.getJSONObject(0).getJSONObject("siswa").getJSONObject("kelas").getString("nama");
                                String tingkat = peminjaman.getJSONObject(0).getJSONObject("siswa").getJSONObject("kelas").getString("tingkat");
                                String cNama = nama + " - " + tingkat + kelas;
                                String tanggal = peminjaman.getJSONObject(0).getString("tanggal_pinjam");
                                String tanggalString = "Tanggal: " + tanggal;
                                itemList.add(new Alat(id,cNama,tanggalString));
                            }
                        }

                        ListDetailAlatAdapter adapter = new ListDetailAlatAdapter(getBaseContext(), itemList);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);

                        recyclerView.setLayoutManager(layoutManager);
                    }else{
                        setContentView(R.layout.pinjam_notfound);
                    }
                    if(itemList.size()<=0){
                        setContentView(R.layout.pinjam_notfound);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("DEBUGS", "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
