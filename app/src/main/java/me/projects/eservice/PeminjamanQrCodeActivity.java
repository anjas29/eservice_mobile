package me.projects.eservice;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.IOException;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class PeminjamanQrCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    public ZXingScannerView mScannerView;
    String nis, nama;
    boolean status = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peminjaman_qr_code);
        nis = getIntent().getStringExtra("nis");
        nama = getIntent().getStringExtra("nama");

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA},
                    100);
        } else {
            mScannerView = new ZXingScannerView(this);
            setContentView(mScannerView);
            status=true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(status){
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(status){
            mScannerView.stopCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mScannerView = new ZXingScannerView(this);
                    setContentView(mScannerView);
                    status=true;
                } else {
                    Toast.makeText(PeminjamanQrCodeActivity.this, "Permission denied to access Camera!", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    @Override
    public void handleResult(Result rawResult) {
        if(status){
            mScannerView.stopCamera();

            Intent intent = new Intent(this,DetailPeminjamanActivity.class);
            intent.putExtra("code", rawResult.getText());
            intent.putExtra("nis", nis);
            intent.putExtra("nama", nama);
            startActivity(intent);

            finish();
        }
    }
}
