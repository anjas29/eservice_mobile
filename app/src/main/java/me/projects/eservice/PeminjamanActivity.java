package me.projects.eservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.projects.eservice.adapter.ListNewsAdapter;
import me.projects.eservice.adapter.Peminjaman;

public class PeminjamanActivity extends AppCompatActivity {
    Button peminjamanButton;
    String nis, nama, kelas;
    TextView namaView,nisView, kelasView, captionView;
    private RecyclerView recyclerView;
    public static ArrayList<Peminjaman> itemList;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peminjaman);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        peminjamanButton = findViewById(R.id.peminjamanButton);
        kelasView = findViewById(R.id.kelasView);
        captionView = findViewById(R.id.captionView);

        nis = getIntent().getStringExtra("nis");

        postCheckNis();

        peminjamanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PeminjamanActivity.this, PeminjamanQrCodeActivity.class);
                intent.putExtra("nis",nis);
                intent.putExtra("nama",nama);
                startActivity(intent);
            }
        });
    }

    public void postCheckNis(){
        //Perhatikan Method.POST dan alamat webservice
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://eservice.gesaang.com/api/siswa",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("DEBUGS", response);
                        try{
                            JSONObject object = new JSONObject(response);
                            boolean status = object.getBoolean("success");

                            if(status){
                                JSONObject data = object.getJSONObject("data");

                                namaView = findViewById(R.id.namaView);
                                nisView = findViewById(R.id.nisView);
                                kelasView = findViewById(R.id.kelasView);

                                nama = data.getString("nama");
                                String kelasNama = data.getJSONObject("kelas").getString("nama");
                                String kelasTingkat = data.getJSONObject("kelas").getString("tingkat");
                                kelas = kelasTingkat + " " + kelasNama;

                                namaView.setText(nama);
                                nisView.setText(data.getString("nis"));
                                kelasView.setText(kelas);
                                itemList = new ArrayList<>();

                                recyclerView = (RecyclerView)findViewById(R.id.listView);
                                layoutManager= new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);

                                JSONArray peminjaman = object.getJSONArray("peminjaman");
                                for(int i=0;i<peminjaman.length();i++){
                                    String nama = peminjaman.getJSONObject(i).getJSONObject("buku").getString("nama");
                                    String tanggal = peminjaman.getJSONObject(i).getString("tanggal_pinjam");
                                    itemList.add(new Peminjaman(nama,tanggal));
                                }

                                if(peminjaman.length()>0){
                                    captionView.setVisibility(View.VISIBLE);
                                }


                                ListNewsAdapter adapter = new ListNewsAdapter(getApplicationContext(), itemList);
                                recyclerView.setAdapter(adapter);
                                recyclerView.setHasFixedSize(true);

                                recyclerView.setLayoutManager(layoutManager);

                            }else{
                                Toast.makeText(PeminjamanActivity.this,"Gagal, "+object.getString("description"),Toast.LENGTH_SHORT).show();
                                setContentView(R.layout.nis_notfound);
                            }

                        }catch (Exception e){
                            Log.d("DEBUGS", e.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("DEBUGS", error.toString());

                    }
                }){
            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("nis", nis);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        postCheckNis();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
