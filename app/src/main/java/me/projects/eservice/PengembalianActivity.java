package me.projects.eservice;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class PengembalianActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    public ZXingScannerView mScannerView;
    boolean status = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengembalian);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.CAMERA},
                    100);
        } else {
            mScannerView = new ZXingScannerView(this);
            setContentView(mScannerView);
            status= true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mScannerView = new ZXingScannerView(this);
                    setContentView(mScannerView);
                    status= true;
                } else {
                    Toast.makeText(PengembalianActivity.this, "Permission denied to access Camera!", Toast.LENGTH_SHORT).show();
                }
                return;
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(status){
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(status){
            mScannerView.stopCamera();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        if(status){
            mScannerView.stopCamera();

            Intent intent = new Intent(this,DetailPengembalianActivity.class);
            intent.putExtra("code", rawResult.getText());
            startActivity(intent);

            finish();
        }
    }
}
