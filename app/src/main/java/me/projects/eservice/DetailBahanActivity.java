package me.projects.eservice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import me.projects.eservice.adapter.Alat;
import me.projects.eservice.adapter.ListDetailAlatAdapter;
import me.projects.eservice.adapter.ListDetailBahanAdapter;
import me.projects.eservice.adapter.PeminjamBahan;

public class DetailBahanActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static ArrayList<PeminjamBahan> itemList;
    private LinearLayoutManager layoutManager;
    String id, nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_bahan);

        recyclerView = (RecyclerView)findViewById(R.id.list_view);
        layoutManager= new LinearLayoutManager(getBaseContext(), LinearLayoutManager.VERTICAL, false);
        id = getIntent().getExtras().getString("id");
        nama = getIntent().getExtras().getString("nama");

        getSupportActionBar().setTitle(nama);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        postCheckDetailBahan();
    }

    public void postCheckDetailBahan(){
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                "http://eservice.gesaang.com/api/list-bahan/"+id, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("DEBUGS", response.toString());
                try {
                    JSONObject data = response.getJSONObject("data");
                    JSONArray bukuArray = data.getJSONArray("buku");
                    JSONObject buku = bukuArray.getJSONObject(0);

                    itemList = new ArrayList<>();
                    if(buku.length()>0){
                        String id = data.getString("id");
                        String satuan = buku.getString("satuan");
                        JSONArray peminjaman = buku.getJSONArray("peminjaman");
                        for(int i=0;i<peminjaman.length();i++){
                            String nama = peminjaman.getJSONObject(i).getJSONObject("siswa").getString("nama");
                            String kelas = peminjaman.getJSONObject(i).getJSONObject("siswa").getJSONObject("kelas").getString("nama");
                            String tingkat = peminjaman.getJSONObject(i).getJSONObject("siswa").getJSONObject("kelas").getString("tingkat");
                            String cNama = nama + " - " + tingkat + " " + kelas;
                            String tanggal = peminjaman.getJSONObject(i).getString("tanggal_pinjam");
                            String jumlah = peminjaman.getJSONObject(i).getString("jumlah");
                            String tanggalString = "Tanggal: " + tanggal;
                            String pengeluaran = "Pengeluaran: " + jumlah + " " + satuan;
                            itemList.add(new PeminjamBahan(id,cNama,tanggalString,pengeluaran));
                        }

                        ListDetailBahanAdapter adapter = new ListDetailBahanAdapter(getBaseContext(), itemList);
                        recyclerView.setAdapter(adapter);
                        recyclerView.setHasFixedSize(true);

                        recyclerView.setLayoutManager(layoutManager);
                    }else{
                        setContentView(R.layout.pinjam_notfound);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("DEBUGS", "Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
