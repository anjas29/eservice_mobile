package me.projects.eservice;

import android.graphics.PorterDuff;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListBarangActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public Fragment alatFragment, mediaFragment, bahanFragment;

    private int[] tabIcons = {
            R.drawable.blue_circle,
            R.drawable.blue_circle,
            R.drawable.blue_circle,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_barang);

        alatFragment = new ListAlatFragment();
        mediaFragment = new ListMediaFragment();
        bahanFragment = new ListBahanFragment();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //setupBluetooth();
        setupTabIcons();

        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.colorPrimary);
                ((ImageView)tab.getCustomView().findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                ((TextView)tab.getCustomView().findViewById(R.id.tabText)).setTextColor(tabIconColor);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.gray);
                ((ImageView)tab.getCustomView().findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                ((TextView)tab.getCustomView().findViewById(R.id.tabText)).setTextColor(tabIconColor);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
    }

    public void setupTabIcons(){
        View tab1 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab1.findViewById(R.id.tabText)).setText("Alat");
        ((ImageView)tab1.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_settings_applications_grey600_48dp);
        int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.colorPrimary);
        ((ImageView)tab1.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab1.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(0).setCustomView(tab1);

        View tab2 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab2.findViewById(R.id.tabText)).setText("Media");
        ((ImageView)tab2.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_movie_grey600_48dp);
        tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.black);
        ((ImageView)tab2.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab2.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(1).setCustomView(tab2);

        View tab3 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab3.findViewById(R.id.tabText)).setText("Bahan");
        ((ImageView)tab3.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_local_gas_station_grey600_48dp);
        tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.black);
        ((ImageView)tab3.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab3.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(2).setCustomView(tab3);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(alatFragment, "Alat");
        adapter.addFragment(mediaFragment, "Media");
        adapter.addFragment(bahanFragment, "Bahan");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
